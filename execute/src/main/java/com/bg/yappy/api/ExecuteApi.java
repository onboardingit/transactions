/**
 * NOTE: This class is auto generated by the swagger code generator program (3.0.24).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package com.bg.yappy.api;

import com.bg.yappy.model.TransactionRequest;
import com.bg.yappy.model.TransactionResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.validation.Valid;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-04T11:55:28.988485600-03:00[America/Santiago]")
public interface ExecuteApi {

    @Operation(summary = "Execute an specific trasaction according to received transaction data.", description = "This service perform many validations in order to verify if a particular can be executed or not. In the case of to be a valid trasaction data, it execute specified trasaction.", tags={ "Transactions" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "201", description = "Transaction executed.", content = @Content(schema = @Schema(implementation = TransactionResponse.class))),
        
        @ApiResponse(responseCode = "400", description = "Balance in source account it's not enough."),
        
        @ApiResponse(responseCode = "409", description = "Account type not supported."),
        
        @ApiResponse(responseCode = "412", description = "Account(s) unable to execute this particular trasaction.") })
    @RequestMapping(value = "/execute",
        produces = { "application/json" }, 
        consumes = { "application/json" }, 
        method = RequestMethod.POST)
    ResponseEntity<TransactionResponse> executeTransaction(@Parameter(in = ParameterIn.DEFAULT, description = "Transaction request object", schema=@Schema()) @Valid @RequestBody TransactionRequest body);


    @Operation(summary = "Health check endpoint.", description = "This endpoint was designed to verify stability of the service.", tags={ "Transactions" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "OK") })
    @RequestMapping(value = "/execute/health-check",
        method = RequestMethod.GET)
    ResponseEntity<Void> healthCheck();

}

