package com.bg.yappy.transactions.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.bg.yappy.model.AccountDTO;
import com.bg.yappy.model.TokenDTO;
import com.bg.yappy.transactions.error.NoValidAccountException;

@Service
public class AccountService {

	private RestService restService;
	
	@Autowired
	public AccountService(RestService restService) {
		this.restService = restService;
	}

	public AccountDTO getAccount(String url, long accountId) {
		url += "/" + accountId;
		return ((RestService<AccountDTO, AccountDTO>) this.restService).postRequest(url, null, AccountDTO.class).getBody();
	}
	
	public TokenDTO validateAccounts(String url, AccountDTO source, AccountDTO destination) {
		Map<String, Long> payload = new HashMap();
		payload.put("source_account_id", source.getId());
		payload.put("destination_account_id", destination.getId());
		try {
			return ((RestService<Map<String, Long>, TokenDTO>) this.restService).postRequest(url, payload, TokenDTO.class).getBody();			
		} catch (HttpClientErrorException e) {
			throw new NoValidAccountException();
		}		
	}
	
	public boolean updateAccountBalance(String url, AccountDTO account) {
		Map<String, AccountDTO> payload = new HashMap();
		payload.put("account", account);
		try {
			((RestService<Map<String, AccountDTO>, Object>) this.restService).postRequest(url, payload, Object.class);
			return true;
		} catch (HttpClientErrorException e) {
			return false;
		}	
	}
	
}
