package com.bg.yappy.transactions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TransactionExecutionServiceApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(TransactionExecutionServiceApplication.class, args);
	}

}
