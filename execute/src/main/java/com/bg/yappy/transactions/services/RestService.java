package com.bg.yappy.transactions.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
public class RestService<I extends Object, O extends Object> {
	
	private final RestTemplate restTemplate;
	private static final String AUTHORIZATION_HEADER = "Authorization";
	
	@Autowired
	public RestService(RestTemplateBuilder builder) {
		this.restTemplate = builder.build();
	}
	
	public ResponseEntity<O> postRequest(String url, I input, Class<O> clazz) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(AUTHORIZATION_HEADER, getBearerToken());
		HttpEntity<I> entity = new HttpEntity<I>(input, headers);
		return this.restTemplate.postForEntity(url, entity, clazz);
	}
	
	public String getBearerToken() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader(AUTHORIZATION_HEADER);
	}

}
