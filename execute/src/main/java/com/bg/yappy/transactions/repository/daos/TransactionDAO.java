package com.bg.yappy.transactions.repository.daos;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;

@Entity
@Table(name = "transaction")
public class TransactionDAO {
	
	@Id
	@Column(length = 50)
	@Getter
	private String id;
	
	@Column(nullable = false)
	@Getter
	private double amount;
			
	@Column(name = "source_account_id", nullable = false)
	@Getter
	private long sourceAccountId;
		
	@Column(name = "destination_account_id", nullable = false)	
	@Getter
	private long destinationAccountId;
		
	@Column(nullable = false, name = "token_id")
	@Getter
	private long tokenId;
	
	@Column(nullable = true)
	@Getter
	private String description;
	
	@Column(name = "creation_date")
	@CreationTimestamp
	@Getter
	private Date creationDate;
	
	public TransactionDAO() {
		super();
		this.id = UUID.randomUUID().toString();
	}
	
	public TransactionDAO(double amount, long sourceAccountId, long destinationAccountId, long tokenId, String description) {
		this();
		this.amount = amount;
		this.sourceAccountId = sourceAccountId;
		this.destinationAccountId = destinationAccountId;
		this.tokenId = tokenId;
		this.description = description;
	}

}
