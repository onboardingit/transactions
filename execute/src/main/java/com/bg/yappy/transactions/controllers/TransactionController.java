package com.bg.yappy.transactions.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.bg.yappy.api.ExecuteApi;
import com.bg.yappy.model.AccountDTO;
import com.bg.yappy.model.FeeOutputDTO;
import com.bg.yappy.model.TokenDTO;
import com.bg.yappy.model.TransactionDTO;
import com.bg.yappy.model.TransactionRequest;
import com.bg.yappy.model.TransactionResponse;
import com.bg.yappy.model.UserDTO;
import com.bg.yappy.transactions.config.ThirdPartyServices;
import com.bg.yappy.transactions.error.UnauthorizedRequestException;
import com.bg.yappy.transactions.repository.daos.TransactionDAO;
import com.bg.yappy.transactions.services.AccountService;
import com.bg.yappy.transactions.services.RestService;
import com.bg.yappy.transactions.services.TransactionService;

@RestController()
public class TransactionController implements ExecuteApi {
	
	private AccountService accountService;
	private TransactionService transactionService;
	private RestService restService;
	
	@Autowired
	private ThirdPartyServices services;
	
	@Autowired
	public TransactionController(AccountService accountService, TransactionService transactionService,
			RestService restService) {
		this.accountService = accountService;
		this.transactionService = transactionService;
		this.restService = restService;
	}

	@Override
	public ResponseEntity<TransactionResponse> executeTransaction(@Valid TransactionRequest transaction) {
		TransactionResponse response = this.transactionService.executeTransaction(transaction);
		return new ResponseEntity(response, HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<Void> healthCheck() {
		return new ResponseEntity(HttpStatus.OK);
	}

}
