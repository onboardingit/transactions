package com.bg.yappy.transactions.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bg.yappy.transactions.repository.daos.TransactionDAO;

public interface TransactionRepository extends JpaRepository<TransactionDAO, String> {

}
