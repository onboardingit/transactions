package com.bg.yappy.transactions.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
public class ThirdPartyServices {
	
	@Value("${services.accounts.list}")
	@Getter
	private String accountList;
	
	@Value("${spring.authentication.validate}")
	@Getter
	private String authenticationValidate;
	
	@Value("${services.transactions.authorization}")
	@Getter
	private String transactionAuthorization;
	
	@Value("${services.accounts.update}")
	@Getter
	private String accountUpdate;
	
	@Value("${services.transactions.fee}")
	@Getter
	private String transactionFee;
	
	@Value("${services.accounts.find}")
	@Getter
	private String accountFind;

}
