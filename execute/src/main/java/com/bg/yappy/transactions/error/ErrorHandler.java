package com.bg.yappy.transactions.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {
	
	@ExceptionHandler(value = NoValidAccountException.class)
	public ResponseEntity<String> invalidAccount() {
		return new ResponseEntity(null, HttpStatus.PRECONDITION_FAILED);
	}
	
	@ExceptionHandler(value = UnauthorizedRequestException.class)
	public ResponseEntity<String> unauthorizedRequest() {
		return new ResponseEntity(null, HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(value = UnsupportedAccountException.class)
	public ResponseEntity<String> unsupportedAccount() {
		return new ResponseEntity(null, HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(value = NoFeesAvailableException.class)
	public ResponseEntity<String> noFees() {
		return new ResponseEntity("{ \"msg\": \"Fees couldn't be calculated\" }", HttpStatus.BAD_REQUEST);
	}
		
	@ExceptionHandler(value = NotEnoughAccountBalanceException.class)
	public ResponseEntity<String> notEnoughBalance() {
		return new ResponseEntity("{ \"msg\": \"Source account hasn't enough balance\" }", HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(value = NotExecutedTransactionException.class)
	public ResponseEntity<String> notExecutedTransaction() {
		return new ResponseEntity("{ \"msg\": \"Transaction execution failed\" }", HttpStatus.BAD_REQUEST);
	}
		
	@ExceptionHandler(value = NotUpdatedAccountBalanceException.class)
	public ResponseEntity<String> notUpdatedBalance() {
		return new ResponseEntity("{ \"msg\": \"Account balance couldn't be updated\" }", HttpStatus.BAD_REQUEST);
	}

}
