package com.bg.yappy.transactions.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.bg.yappy.model.AccountDTO;
import com.bg.yappy.model.Fee;
import com.bg.yappy.model.FeeInputDTO;
import com.bg.yappy.model.FeeOutputDTO;
import com.bg.yappy.model.TokenDTO;
import com.bg.yappy.model.TransactionDTO;
import com.bg.yappy.model.TransactionRequest;
import com.bg.yappy.model.TransactionResponse;
import com.bg.yappy.model.UserDTO;
import com.bg.yappy.transactions.config.ThirdPartyServices;
import com.bg.yappy.transactions.error.NoFeesAvailableException;
import com.bg.yappy.transactions.error.NotEnoughAccountBalanceException;
import com.bg.yappy.transactions.error.NotExecutedTransactionException;
import com.bg.yappy.transactions.error.NotUpdatedAccountBalanceException;
import com.bg.yappy.transactions.error.UnauthorizedRequestException;
import com.bg.yappy.transactions.error.UnsupportedAccountException;
import com.bg.yappy.transactions.repository.TransactionRepository;
import com.bg.yappy.transactions.repository.daos.TransactionDAO;

@Service
public class TransactionService {

	private final TransactionRepository transactionRepo;
	private final RestService restService;
	private final AccountService accountService;
	
	@Autowired
	private ThirdPartyServices services;

	@Autowired
	public TransactionService(TransactionRepository transactionRepo, RestService restService, AccountService accountService) {
		this.transactionRepo = transactionRepo;
		this.restService = restService;
		this.accountService = accountService;
	}

	public TransactionDAO save(TransactionDAO transaction) {
		return this.transactionRepo.save(transaction);
	}

	public void delete(TransactionDAO transaction) {
		this.transactionRepo.delete(transaction);
	}

	private FeeOutputDTO getFees(String url, FeeInputDTO inputFee) {
		try {
			return ((RestService<FeeInputDTO, FeeOutputDTO>) this.restService)
					.postRequest(url, inputFee, FeeOutputDTO.class).getBody();
		} catch (HttpClientErrorException e) {
			if (e.getRawStatusCode() == 409) {
				throw new UnsupportedAccountException();
			}
			throw new NoFeesAvailableException();
		} catch (Exception e) {
			throw new UnauthorizedRequestException();
		}
	}

	public FeeOutputDTO getFeeData(String url, String accountType, double transactionAmount) {
		return this.getFees(url, new FeeInputDTO(accountType, transactionAmount));
	}

	public UserDTO getUserData(String url, UserDTO input) {
		try {
			return ((RestService<UserDTO, UserDTO>) this.restService).postRequest(url, input, UserDTO.class).getBody();
		} catch (HttpClientErrorException e) {
			throw new UnauthorizedRequestException();
		}
	}

	public UserDTO validateToken(String url, TokenDTO input) {
		try {
			return ((RestService<TokenDTO, UserDTO>) this.restService).postRequest(url, input, UserDTO.class).getBody();
		} catch (HttpClientErrorException e) {
			throw new UnauthorizedRequestException();
		}
	}
	
	private boolean updateBalance(AccountDTO source, AccountDTO destination) {
		return this.accountService.updateAccountBalance(this.services.getAccountUpdate(), source) 
				&& this.accountService.updateAccountBalance(this.services.getAccountUpdate(), destination);
	}
	
	public TransactionResponse executeTransaction(TransactionRequest transaction) {
		TokenDTO token = new TokenDTO(this.restService.getBearerToken());
		UserDTO user = this.validateToken(this.services.getAuthenticationValidate(), token);
		AccountDTO sourceAccount = this.getSourceAccount(token, user, transaction.getDestinationAccountId());
		AccountDTO destinationAccount = this.accountService.getAccount(this.services.getAccountFind(), transaction.getDestinationAccountId());
		TokenDTO transactionToken = this.accountService.validateAccounts(this.services.getTransactionAuthorization(), sourceAccount,
				destinationAccount);
		Fee fee = this.getFee(transaction, sourceAccount);
		TransactionDTO transactionDto = new TransactionDTO(transaction);
		transactionDto.setSourceAccountId(sourceAccount.getId());
		transactionDto.setAmount(fee.getAmount());
		TransactionDAO tr = null;
		try {
			tr = this.save(transactionDto.toDomain());
		} catch (Exception e) {
			throw new NotExecutedTransactionException();
		}
		sourceAccount.setBalanceAfterTransaction(tr);
		destinationAccount.setBalanceAfterTransaction(tr);
		if (!this.updateBalance(sourceAccount, destinationAccount)) {
			this.delete(tr);
			throw new NotUpdatedAccountBalanceException();
		}
		TransactionResponse response = new TransactionResponse();
		response.setTotalAmount(tr.getAmount());
		response.setFee(fee);
		response.setTransactionToken(transactionToken.getToken());
		return response;
	}
	
	private AccountDTO getSourceAccount(TokenDTO token, UserDTO user, long destinationAccountId) {
		UserDTO userFullData = this.getUserData(this.services.getAccountList(), user);
		AccountDTO sourceAccount = userFullData.getAccounts().stream().filter(acc -> acc.isYappyValid()).findFirst().get();
		if (sourceAccount == null || sourceAccount.getId() == destinationAccountId) {
			throw new UnsupportedAccountException();
		}
		return sourceAccount;
	}
	
	private Fee getFee(TransactionRequest transaction, AccountDTO account) {
		FeeOutputDTO incomingFee = this.getFeeData(this.services.getTransactionFee(), account.getType(), transaction.getAmount());
		if (account.getBalance() < incomingFee.getAmount()) {
			throw new NotEnoughAccountBalanceException();
		}
		return incomingFee.toFee();
	}

}
