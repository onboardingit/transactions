package com.bg.yappy.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class UserDTO {
	
	@JsonProperty("user_id")
	@Getter
	private long id;
	
	@JsonProperty("email")
	@Getter
	private String email;
	
	@JsonProperty("first_name")
	@Getter
	private String firstName;
	
	@JsonProperty("last_name")
	@Getter
	private String lastName;
	
	@JsonProperty("identity_id")
	@Getter
	private String identityId;
	
	@JsonProperty("identity_type")
	@Getter
	private String identityType;
	
	@JsonProperty("password")
	@Getter
	private String password;
	
	@JsonProperty("phone_number")
	@Getter
	private String phone;
	
	@JsonProperty("data")
	@Getter
	private List<AccountDTO> accounts;	

	public UserDTO(long id) {
		super();
		this.id = id;
	}
	
	public UserDTO(long id, String email, String firstName, String lastName, String identityId, String identityType,
			String password, String phone) {
		this(id);		
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.identityId = identityId;
		this.identityType = identityType;
		this.password = password;
		this.phone = phone;
	}

}
