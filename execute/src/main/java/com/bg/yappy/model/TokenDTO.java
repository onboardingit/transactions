package com.bg.yappy.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class TokenDTO {
	
	@JsonProperty("token")
	@Getter
	private String token;
	
	@JsonProperty("token_id")
	@Getter
	private long tokenId;
	
	@JsonProperty("expiration_date")
	@Getter
	private Date expirationDate;
	
	@JsonProperty("request_date")
	@Getter
	private Date requestDate;
	
	public TokenDTO(String token) {
		this.token = token;
	}
	
	public TokenDTO(String token, Date expirationDate, Date requestDate) {
		super();
		this.token = token;
		this.expirationDate = expirationDate;
		this.requestDate = requestDate;
	}
	
}
