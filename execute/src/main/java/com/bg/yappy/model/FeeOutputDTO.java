package com.bg.yappy.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class FeeOutputDTO {
	
	@JsonProperty("total_amount")
	@Getter
	private double amount;
	
	@JsonProperty("percentage")
	@Getter
	private double percentage;
	
	@JsonProperty("fee_amount")
	@Getter
	private double fee;
	
	public FeeOutputDTO(double amount, double percentage, double fee) {
		this.amount = amount;
		this.percentage = percentage;
		this.fee = fee;
	}
	
	public Fee toFee() {
		Fee fee = new Fee();
		fee.setAmount(this.amount);
		fee.setPercentage(this.percentage);
		return fee;
	}

}
