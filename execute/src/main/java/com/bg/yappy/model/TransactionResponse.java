package com.bg.yappy.model;

import java.util.Objects;
import com.bg.yappy.model.Fee;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TransactionResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-04T11:55:28.988485600-03:00[America/Santiago]")


public class TransactionResponse   {
  @JsonProperty("total_amount")
  private Double totalAmount = null;

  @JsonProperty("fee")
  private Fee fee = null;

  @JsonProperty("transaction_token")
  private String transactionToken = null;

  public TransactionResponse totalAmount(Double totalAmount) {
    this.totalAmount = totalAmount;
    return this;
  }

  /**
   * Get totalAmount
   * @return totalAmount
   **/
  @Schema(description = "")
  
    public Double getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(Double totalAmount) {
    this.totalAmount = totalAmount;
  }

  public TransactionResponse fee(Fee fee) {
    this.fee = fee;
    return this;
  }

  /**
   * Get fee
   * @return fee
   **/
  @Schema(description = "")
  
    @Valid
    public Fee getFee() {
    return fee;
  }

  public void setFee(Fee fee) {
    this.fee = fee;
  }

  public TransactionResponse transactionToken(String transactionToken) {
    this.transactionToken = transactionToken;
    return this;
  }

  /**
   * Get transactionToken
   * @return transactionToken
   **/
  @Schema(description = "")
  
    public String getTransactionToken() {
    return transactionToken;
  }

  public void setTransactionToken(String transactionToken) {
    this.transactionToken = transactionToken;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransactionResponse transactionResponse = (TransactionResponse) o;
    return Objects.equals(this.totalAmount, transactionResponse.totalAmount) &&
        Objects.equals(this.fee, transactionResponse.fee) &&
        Objects.equals(this.transactionToken, transactionResponse.transactionToken);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalAmount, fee, transactionToken);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransactionResponse {\n");
    
    sb.append("    totalAmount: ").append(toIndentedString(totalAmount)).append("\n");
    sb.append("    fee: ").append(toIndentedString(fee)).append("\n");
    sb.append("    transactionToken: ").append(toIndentedString(transactionToken)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
