package com.bg.yappy.model;

import com.bg.yappy.transactions.repository.daos.TransactionDAO;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class AccountDTO {
	
	@JsonProperty("id")
	@Getter
	public long id;
	
	@JsonProperty("account_number")
	@JsonAlias("accountNumber")
	@Getter
	public String accountNumber;
	
	@JsonProperty("account_status")
	@Getter
	public String accountStatus;
	
	@JsonProperty("balance")
	@Getter
	public double balance;
	
	@JsonProperty("account_type")
	@Getter
	public String type;
	
	public AccountDTO(long id) {
		this.id = id;
	}
	
	public AccountDTO(long id, String accountNumber, String accountStatus, double balance, String type) {
		this(id);		
		this.accountNumber = accountNumber;
		this.accountStatus = accountStatus;
		this.balance = balance;
		this.type = type;
	}
	
	public boolean isYappyValid() {
		return this.getAccountStatus().equals("YAPPY");
	}
	
	public void setBalanceAfterTransaction(TransactionDAO transaction) {
		if (this.getId() == transaction.getSourceAccountId()) {
			this.balance -= transaction.getAmount();
		} else if (this.getId() == transaction.getDestinationAccountId()) {
			this.balance += transaction.getAmount();
		}
	}
	
}

