package com.bg.yappy.model;

import com.bg.yappy.transactions.repository.daos.TransactionDAO;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

public class TransactionDTO {
	
	@JsonProperty("source_account_id")
	@Getter
	@Setter
	private long sourceAccountId;
	
	@JsonProperty("destination_account_id")
	@Getter
	private long destinationAccountId;
	
	@JsonProperty("amount")
	@Getter
	@Setter
	private double amount;
	
	@JsonProperty("description")
	@Getter
	private String description;
	
	@JsonProperty("token_id")
	@Getter
	@Setter
	private long tokenId;
	
	public TransactionDTO(long sourceAccountId, long destinationAccountId, double amount, String description, long tokenId) {
		this.sourceAccountId = sourceAccountId;
		this.destinationAccountId = destinationAccountId;
		this.amount = amount;
		this.description = description;
		this.tokenId = tokenId;
	}
	
	public TransactionDTO(TransactionRequest transaction) {
		this.amount = transaction.getAmount();
		this.destinationAccountId = transaction.getDestinationAccountId();
		this.description = transaction.getDescription();
	}

	public TransactionDAO toDomain() {		
		return new TransactionDAO(this.amount, this.sourceAccountId, this.destinationAccountId, this.tokenId, this.description);
	}

}
