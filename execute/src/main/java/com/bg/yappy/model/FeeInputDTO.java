package com.bg.yappy.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class FeeInputDTO {
	
	@JsonProperty("account_type")
	@Getter
	private String accountType;
	
	@JsonProperty("amount")
	@Getter
	private double amount;
	
	public FeeInputDTO(String accountType, double amount) {
		this.accountType = accountType;
		this.amount = amount;
	}

}
